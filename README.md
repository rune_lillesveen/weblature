Weblature
=========

A javascript library for fretted instruments.

Current functionality
---------------------

Creating objects for fretted instruments with an arbitrary number of strings,
arbitrary tunings, and possibility to add a capo. Fingerings with open and
muted strings can be specified and a list of possible chord names is generated.

Dependencies
------------

Uses qunit for testing, but the library itself does not rely on any javascript
frameworks.

License
-------

MIT License.

