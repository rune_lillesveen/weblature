/*
    Weblature

    Javascript library for chords and tablature.
    (for fretted string instruments)


    Copyright (C) 2013 by Rune Lillesveen

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

Weblature.addTest(function(){

    var guitar = new Weblature.Guitar;

    module("Weblature.Instrument.getChordNames for Weblature.Guitar");

    test("x02210 -> Am", function() {
        var chords = guitar.getChordNames([-1,0,2,2,1,0]);
	ok(chords, "chords defined");
	equal(chords[2], "Am");
    });

    test("x32310 -> C7", function() {
        var chords = guitar.getChordNames([-1,3,2,3,1,0]);
	ok(chords, "chords defined");
	equal(chords[0], "C7");
    });

    test("x32000 -> Cmaj7", function() {
        var chords = guitar.getChordNames([-1,3,2,0,0,0]);
	ok(chords, "chords defined");
	equal(chords[0], "Cmaj7");
    });

    test("x35343 -> Cm7", function() {
        var chords = guitar.getChordNames([-1,3,5,3,4,3]);
	ok(chords, "chords defined");
	equal(chords[0], "Cm7");
    });

    test("x32311 -> C7add11", function() {
        var chords = guitar.getChordNames([-1,3,2,3,1,1]);
	ok(chords, "chords defined");
	equal(chords[0], "C7add11");
    });

    test("x32333 -> C9", function() {
        var chords = guitar.getChordNames([-1,3,2,3,3,3]);
	ok(chords, "chords defined");
	equal(chords[0], "C9");
    });

    test("x30333 -> C9sus2", function() {
        var chords = guitar.getChordNames([-1,3,0,3,3,3]);
	ok(chords, "chords defined");
	equal(chords[0], "C9sus2");
    });

    test("x54030 -> Dadd9add11", function() {
        var chords = guitar.getChordNames([-1,5,4,0,3,0]);
	ok(chords, "chords defined");
	equal(chords[0], "Dadd9add11");
    });

    test("x02200 -> Asus2", function() {
        var chords = guitar.getChordNames([-1,0,2,2,0,0]);
	ok(chords, "chords defined");
	equal(chords[1], "Asus2");
    });

    test("x32110 -> Caug", function() {
	var chords = guitar.getChordNames([-1,3,2,1,1,0])
	ok(chords, "chords defined");
	equal(chords[0], "Caug");
    });

    test("x32116 -> Caug7", function() {
	var chords = guitar.getChordNames([-1,3,2,1,1,6])
	ok(chords, "chords defined");
	equal(chords[0], "Caug7");
    });

    test("880100 -> Caugmaj11", function() {
	var chords = guitar.getChordNames([8,8,0,1,0,0])
	ok(chords, "chords defined");
	equal(chords[0], "Caugmaj11");
    });

    test("x32210 -> C6", function() {
	var chords = guitar.getChordNames([-1,3,2,2,1,0])
	ok(chords, "chords defined");
	equal(chords[0], "C6");
    });

    test("x35555 -> C6", function() {
	var chords = guitar.getChordNames([-1,3,5,5,5,5])
	ok(chords, "chords defined");
	equal(chords[0], "C6");
    });

    test("x30035 -> C6sus2", function() {
	var chords = guitar.getChordNames([-1,3,0,0,3,5])
	ok(chords, "chords defined");
	equal(chords[0], "C6sus2");
    });

    test("x355xx -> C5", function() {
	var chords = guitar.getChordNames([-1,3,5,5,-1,-1])
	ok(chords, "chords defined");
	equal(chords[0], "C5");
    });

    test("202210 -> Am/F#", function() {
	var chords = guitar.getChordNames([2,0,2,2,1,0])
	ok(chords, "chords defined");
	equal(chords[3], "Am/F#");
    });

    test("032010 -> C/E", function() {
	var chords = guitar.getChordNames([0,3,-1,0,1,-1])
	ok(chords, "chords defined");
	equal(chords[0], "C/E");
    });

    test("xx101x -> Cm/D#", function() {
	var chords = guitar.getChordNames([-1,-1,1,0,1,-1])
	ok(chords, "chords defined");
	equal(chords[0], "Cm/D#");
    });

    var mandolin = new Weblature.Mandolin;

    module("Weblature.Instrument.getChordNames for Weblature.Mandolin");

    test("2002 -> D/A", function() {
	var chords = mandolin.getChordNames([2,0,0,2]);
	ok(chords, "chords defined");
	equal(chords[0], "D/A");
    });

    test("0230 -> C/G", function() {
	var chords = mandolin.getChordNames([0,2,3,0]);
	ok(chords, "chords defined");
	equal(chords[0], "C/G");
    });
});
