/*
    Weblature

    Javascript library for chords and tablature.
    (for fretted string instruments)


    Copyright (C) 2013 by Rune Lillesveen

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

Weblature.addTest(function(){

    module("Weblature.NoteString");

    test("Sharp scale", function() {
		var exp = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" ];
		for (var i=0; i<24; i++)
			equal(Weblature.NoteString(i, false, false), exp[i % 12]);
    });

    test("Flat scale", function() {
		var exp = ["C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B" ];
		for (var i=0; i<24; i++)
			equal(Weblature.NoteString(i, false, true), exp[i % 12]);
    });

    test("Sharp scale with octave", function() {
		var exp = ["C2", "C#2", "D2", "D#2", "E2", "F2", "F#2", "G2", "G#2", "A2", "A#2", "B2" ];
		for (var i=24; i<36; i++)
			equal(Weblature.NoteString(i, true, false), exp[i % 12]);
    });

    test("Flat scale with octave", function() {
		var exp = ["C4", "Db4", "D4", "Eb4", "E4", "F4", "Gb4", "G4", "Ab4", "A4", "Bb4", "B4" ];
		for (var i=48; i<60; i++)
			equal(Weblature.NoteString(i, true, true), exp[i % 12]);
    });

});
