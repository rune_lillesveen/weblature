/*
    Weblature

    Javascript library for chords and tablature.
    (for fretted string instruments)


    Copyright (C) 2013 by Rune Lillesveen

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

Weblature.addTest(function(){

    var guitar = new Weblature.Guitar;

    module("Weblature.Instrument.getSemitones");

    test("All strings muted", function() {
        deepEqual(guitar.getSemitones([-1,-1,-1,-1,-1,-1]), [0,0,0,0,0,0,0,0,0,0,0,0], "Compare with expected semitone array");
    });

    test("All strings open", function() {
        deepEqual(guitar.getSemitones([0,0,0,0,0,0]), [0,0,1,0,2,0,0,1,0,1,0,1], "Compare with expected semitone array");
    });

    test("All strings open - Capo on third fret", function() {
        equal(guitar.getCapoPosition(), 0, "Check initial capo position")
        guitar.setCapoPosition(3);
        deepEqual(guitar.getSemitones([0,0,0,0,0,0]), [1,0,1,0,0,1,0,2,0,0,1,0], "Compare with expected semitone array");
        guitar.setCapoPosition(0);
    });

    test("Standard C chord", function() {
        deepEqual(guitar.getSemitones([-1,3,2,0,1,0]), [2,0,0,0,1,0,0,1,0,0,0,0], "Compare with expected semitone array");
    });

    test("D chord - C shape with capo on second", function() {
        guitar.setCapoPosition(2);
        deepEqual(guitar.getSemitones([-1,3,2,0,1,0]), [0,0,2,0,0,0,1,0,0,1,0,0], "Compare with expected semitone array");
        guitar.setCapoPosition(0);
    });

});
