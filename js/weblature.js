/*
    Weblature

    Javascript library for chords and tablature.
    (for fretted string instruments)


    Copyright (C) 2013 by Rune Lillesveen

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/


/* Weblature namespace. */

Weblature = {

    /* Utility function for sub-classing through prototype chaining. */

    extend: function(baseConstructor) {
        var intermediateConstructor = function() {};
        intermediateConstructor.prototype = baseConstructor.prototype;
        return new intermediateConstructor;
    },

    /* Utility function for (a div b). */

    div: function(a, b) {
        return Math.floor(a/b);
    },

    /* Mapping from note string to semitone index. */

    "C"  : 0,
    "C#" : 1,
    "Db" : 1,
    "D"  : 2,
    "D#" : 3,
    "Eb" : 3,
    "E"  : 4,
    "F"  : 5,
    "F#" : 6,
    "Gb" : 6,
    "G"  : 7,
    "G#" : 8,
    "Ab" : 8,
    "A"  : 9,
    "A#" : 10,
    "Bb" : 10,
    "B"  : 11
}

/* Return the semitone number for a named note in a given octave.

   noteString - one of the strings in the mapping above. Case-sensitive.
   octave - integer, 0 or positive, to indicate octave. */

Weblature.Note = function(noteString, octave) {
    return Weblature[noteString] + octave*12;
}

/* Return the string representation of a semitone value created by the Note
   function above.

   note - the semitone value.
   includeOctave - add the octave number to the string. For instance "E4"
                   instead of simply "E".
   flat - If true, choose a flat name in favour of a sharp.
          E.g. Ab instead of G#. */

Weblature.NoteString = function(note, includeOctave, flat) {

    var idx = note % 12;
    var leastF = idx >= 5;
    var sharpOrFlat = ((idx % 2) == 0) == leastF;
    var offset = Weblature.div(idx, 2);

    if (sharpOrFlat && flat ||
        !sharpOrFlat && leastF)
        offset++;

    var retStr = String.fromCharCode("A".charCodeAt(0) + ((offset + 2) % 7));

    if (sharpOrFlat)
        retStr += flat ? "b" : "#";

    if (includeOctave == true)
        retStr += Weblature.div(note, 12);

    return retStr;
}

/* Constructor for creating an Instrument object with a given tuning.
   The number of strings is implicitly given by the length of the tuning array.

   tuning - the tuning (see Weblature.setTuning). */

Weblature.Instrument = function(tuning) {

    this.setTuning(tuning);
    this.setCapoPosition(0);
}

/* Instrument class prototype. */

Weblature.Instrument.prototype = {

    /* Set the tuning (and implicitly the number of strings) for the
       instrument.

       tuning - an array of semitones. The array entries should be
                ordered with the string with the heaviest gauge as
                the first array element. The elements of the array
                should be created using the Weblature.Note method. */

    setTuning: function(tuning) {
        if (this.tuning && this.tuning.length != tuning.length)
            throw new Object;/*Weblature.StringCountException;*/
        this.stringCount = tuning.length;
        this.tuning = tuning;
    },

    /* Adjust the tuning with a relative number of semitones up or
       down for each string. Negative numbers to tune down.

       relativeTuning - an array with an integer entry for each string
                        giving the number of semitones to tune the string up
                        or down. */

    setRelativeTuning: function(relativeTuning) {
        if (this.tuning.length != relativeTuning.length)
            throw new Object;/*Weblature.StringCountException;*/
        this.relativeTuning = relativeTuning;
    },

    /* Return the number of strings that this instrument has. */

    getStringCount: function() {
        return this.stringCount;
    },

    /**/

    printTuning: function() {
        var str = "";
        for (i=0; i<this.stringCount; i++)
            str += Weblature.NoteString(this.tuning[i]);
        return str;
    },

    /* Get the capo position. 0 means no capo, 1 means first fret, etc. */

    getCapoPosition: function() {
        return this.capo;
    },

    /* Set capo position. 0 means no capo, 1 means first fret, etc.

       fret - the fret position. */

    setCapoPosition: function(fret) {
        this.capo = fret;
    },

    /* Find the semitones played for a given fingering. */

    getSemitones: function(fingering) {
        var semitones = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
        var bassNote;

        if (fingering.length != this.stringCount)
            throw new Object;/*StringCountException;*/

        for (var i = 0; i < this.stringCount; i++) {
            if (fingering[i] >= 0) {
                var semitone = this.tuning[i] + this.capo + fingering[i];
                if (this.relativeTuning)
                    semitone += this.relativeTuning[i];
                if (bassNote == undefined || semitone < bassNote)
                    bassNote = semitone;
                semitones[semitone % 12] = 1;
            }
        }

        if (bassNote != undefined) {
            bassNote = bassNote % 12;
            semitones[bassNote] = 2;
        }

        return semitones;
    },

    /* Return an array of possible names for the chord given the fingering,
       the current tuning, and the current capo position.

       fingering - array of finger positions relative to capo position. */

    getChordNames: function(fingering) {

        var chordNames = new Array;
        var next = 0;
        var bassIndex = -1;

        var semitones = this.getSemitones(fingering);

        for (var i = 0; i < 12; i++)
            if (semitones[i] == 2)
                bassIndex = i;

        for (var root = 0; root < 12; root++) {
            if (semitones[root] > 0) {
                var third = "";
                var fifth = "";
                var seventh = "";
                var sus = "";
                var ext = 0;
                var adds = "";

                /* Major, minor, suspended, or no third? */
                if (semitones[(root+4)%12] == 0) {
                    if (semitones[(root+3)%12] != 0)
                        third = "m";
                    else {
                        if (semitones[(root+2)%12] != 0)
                            sus += "sus2";
                        if (semitones[(root+5)%12] != 0)
                            sus += "sus4";
                        if (sus == "")
                            ext = 5;
                    }
                }

                /* Diminished, augmented, or no fifth? */
                if (semitones[(root+7)%12] == 0) {
                    if (semitones[(root+6)%12] != 0)
                        fifth = "dim";
                    else if (semitones[(root+8)%12] != 0)
                        fifth = "aug";
                }

                /* Extended chords. */
                if (semitones[(root+10)%12] != 0)
                    ext = 7;
                else if (semitones[(root+11)%12] != 0) {
                    seventh = "maj";
                    ext = 7;
                }

                if (semitones[(root+2)%12] != 0)
                    if (ext == 7)
                        ext = 9;
                    else if (!sus.match("sus2"))
                        adds += "add9";

                if (semitones[(root+5)%12] != 0)
                    if (ext == 9)
                        ext = 11;
                    else if (!sus.match("sus4"))
                        adds += "add11";

                if (semitones[(root+9)%12] != 0)
                    if (ext == 11)
                        ext = 13;
                    else if (ext < 7)
                        ext = 6;
                    else
                        adds += "add13";

                /* Root name + seventh + extension + third + fifth + adds + bass */

                var chordString = Weblature.NoteString(root, false, false);

                chordString += third;
                chordString += fifth;
                chordString += seventh;

                if (ext > 0)
                    chordString += ext.toString();

                chordString += sus;
                chordString += adds;

                /* TODO: If the bass note is not the root note, and the chord
                   name would be simpler without the bass note, remove the bass
                   note from the chord. For instance Am6/F# could be written
                   Am/F#. */

                if (root != bassIndex)
                    chordString += "/" + Weblature.NoteString(bassIndex, false, false);

                chordNames[next++] = chordString;
            }
        }

        return chordNames;
    }
}

/* Constructor for creating a Guitar.

   Initially, it gets the standard EADGBE tuning. */

Weblature.Guitar = function() {

    var guitarTuning = [
        Weblature.Note("E", 2),
        Weblature.Note("A", 2),
        Weblature.Note("D", 3),
        Weblature.Note("G", 3),
        Weblature.Note("B", 3),
        Weblature.Note("E", 4)
    ];

    Weblature.Instrument.call(this, guitarTuning);
}

/* Sub-class Guitar from Instrument. */

Weblature.Guitar.prototype = Weblature.extend(Weblature.Instrument);

/* Constructor for creating a Mandolin.

   Initially, it gets the standard GDAE tuning. */

Weblature.Mandolin = function() {

    var mandolinTuning = [
        Weblature.Note("G", 3),
        Weblature.Note("D", 4),
        Weblature.Note("A", 4),
        Weblature.Note("E", 5)
    ];

    Weblature.Instrument.call(this, mandolinTuning);
}

/* Sub-class Mandolin from Instrument. */

Weblature.Mandolin.prototype = Weblature.extend(Weblature.Instrument);

